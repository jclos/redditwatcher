package rgu.jclos.util ;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 * @param <T> The type of the elements stored in the Accumulator
 */
public class Accumulator<T extends Number> implements Comparator<T>  {
      
    private final ArrayList<T> _internal ;
    private T max = null;
    private double maxValue ;
    private T min = null;
    private double minValue ;
    private boolean filled ;
    
    /**
     * Constructor method
     * @param <T> type of data accumulated
     */
    public <T extends Number> Accumulator()
    {
        filled = false ;
        _internal = new ArrayList();
        minValue = Double.MAX_VALUE ;
        maxValue = Double.MIN_VALUE ;
    }
    
    /**
     * Returns the numbers stored as a list
     * @return a list
     */
    public List<T> getListOfElements()
    {
        return _internal;
    }
    
    /**
     * Returns the most frequent element of the Accumulator
     * @return the most frequent element of the Accumulator
     */
    public T getMostFrequentElement()
    {
        Map<T, Integer> index = new HashMap<>();
        for (T element : _internal)
        {
            int previous = index.getOrDefault(element, 0);
            index.put(element, previous + 1);
        }
        int maxF = Integer.MIN_VALUE;
        T maxElement = null;
        for (T element : index.keySet())
        {
            int f = index.getOrDefault(element, 0);
            if (f >= maxF)
            {
                maxElement = element ;
                maxF = f ;
            }
        }
        return maxElement ;
    }
    
    /**
     * Returns the getAverage of the positive numbers stored
     * @return the getAverage, as a double
     */
    public double getAverageOfPositiveElements()
    {
        double d = 0d;
        d = _internal.stream().filter(element -> element.doubleValue() >= 0).map((element) -> element.doubleValue()).reduce(d, (accumulator, _item) -> accumulator + _item);
        return (_internal.isEmpty()?0:d/_internal.size()); 
    }
    
    /**
     * Returns the getSumOfElements of the positive numbers stored
     * @return the getSumOfElements, as a double
     */
    public double getSumOfPositiveElements()
    {
        double d = 0d;
        d = _internal.stream().filter(element -> element.doubleValue() >= 0).map((element) -> element.doubleValue()).reduce(d, (accumulator, _item) -> accumulator + _item);
        return (_internal.isEmpty()?0:d);  
    }
    
    /**
     * Returns the getAverage of negative data stored
     * @return the getAverage, as a double
     */
    public double getAverageOfNegativeElements()
    {
        double d = 0d;
        d = _internal.stream().filter(element -> element.doubleValue() <= 0).map((element) -> element.doubleValue()).reduce(d, (accumulator, _item) -> accumulator + _item);
        return (_internal.isEmpty()?0:d/_internal.size());   
    }
    
    /**
     * Returns the number of negative numbers stored
     * @return the number of negative numbers stored, as an integer
     */
    public int getCountOfNegativeElements()
    {
        int c = 0 ;
        c = _internal.stream().filter((element) -> (element.doubleValue() <= 0)).map((_item) -> 1).reduce(c, Integer::sum);
        return c ;
    }
    
    /**
     * Returns the number of positive numbers stored
     * @return the number of positive numbers stored, as an integer
     */
    public int getCountOfPositiveElements()
    {
        int c = 0 ;
        c = _internal.stream().filter((element) -> (element.doubleValue() >= 0)).map((_item) -> 1).reduce(c, Integer::sum);
        return c ;
    }

    /**
     * Returns the getSumOfElements of the negative numbers stored
     * @return the getSumOfElements, as a double
     */
    public double getSumOfNegativeElements()
    {
        double d = 0d;
        d = _internal.stream().filter(element -> element.doubleValue() <= 0).map((element) -> element.doubleValue()).reduce(d, (accumulator, _item) -> accumulator + _item);
        return (_internal.isEmpty()?0:d);   
    }
    
    /**
     * Returns the numbers stored as a sorted list
     * @param ascending whether the list should be stored in an ascending manner
     * @return a sorted list of the numbers stored
     */
    public List<T> getSortedList(boolean ascending)
    {
        Collections.sort(_internal, this);
        if (ascending) return _internal;
        else
        {
            Collections.reverse(_internal);
            return _internal ;
        }
    }
   
    /**
     * Returns true if the accumulator is empty
     * @return 
     */
    public boolean isEmpty()
    {
        return _internal.isEmpty();
    }
    
    /**
     * Adds the element to the accumulator
     * @param element 
     */
    public void add(T element)
    {
        _internal.add(element);
        if (!filled || element.doubleValue() >= this.maxValue) {
            this.max = element ;
            this.maxValue = element.doubleValue() ;
            filled = true ;
        }
        if (!filled || element.doubleValue() <= this.minValue) {
            this.min = element ;
            this.minValue = element.doubleValue() ;
            filled = true ;
        }
    }
    
    /**
     * Adds the element to the accumulator
     * @param element 
     */
    public void add(T element, Predicate<T> filter)
    {
        if (filter.test(element)) {
            _internal.add(element);
            if (!filled || element.doubleValue() >= this.maxValue) {
                this.max = element ;
                this.maxValue = element.doubleValue() ;
                filled = true ;
            }
            if (!filled || element.doubleValue() <= this.minValue) {
                this.min = element ;
                this.minValue = element.doubleValue() ;
                filled = true ;
            }    
        }
    }
    
    /**
     * Returns the getAverage of the elements stored
     * @return getAverage of the elements stored, as a double
     */
    public double getAverage()
    {
        double d = 0d;
        d = _internal.stream().map((element) -> element.doubleValue()).reduce(d, (accumulator, _item) -> accumulator + _item);
        return (_internal.isEmpty()?0:d/_internal.size());
    }
    
    /**
     * Returns the getProduct of the elements stored.
     * @param eta if 0, returns a simple getProduct. Otherwise applies a laplacian smoothing by adding eta to each getCountOfElements
     * @return the getProduct of the elements stored 
     */
    public double getProduct(double eta)
    {
        double d = 0d;
        if (eta > 0)
        {
            d = _internal.stream().map((element) -> element.doubleValue()).reduce(d, (accumulator, _item) -> accumulator * (_item + eta));
        }
        else {
            d = _internal.stream().map((element) -> element.doubleValue()).reduce(d, (accumulator, _item) -> accumulator * _item);
        }
        return (_internal.isEmpty()?0:d);
    }
    
    /**
     * Returns the getProduct of the elements stored.
     * @param eta if 0, returns a simple getProduct. Otherwise applies a laplacian smoothing by adding eta to each getCountOfElements
     * @return the logproduct of the elements stored 
     */
    public double getLogProduct(double eta)
    {
        double d = 0d;
        d = _internal.stream().map((element) -> Math.log10(element.doubleValue() + eta)).reduce(d, (accumulator, _item) -> accumulator + _item);
        return (_internal.isEmpty() ? 0 : d);
    }
    
    
    /**
     * Returns the getAverage of the majority class (positives or negatives)
     * @return getAverage of the majority class (positives or negatives), as a double
     */
    public double getAverageOfMajority()
    {
        Accumulator negatives = new Accumulator();
        Accumulator positives = new Accumulator();
        _internal.stream().forEach((element) -> {
            if (element.doubleValue() <= 0d)
            {
                negatives.add(element);
            }
            else if (element.doubleValue() >= 0d)
            {
                positives.add(element);
            }
        });
        if (negatives.getCountOfElements() > positives.getCountOfElements())
        {
            return negatives.getAverage();
        }
        else if (positives.getCountOfElements() > negatives.getCountOfElements())
        {
            return positives.getAverage();
        }
        else
        {
            if (positives.getSumOfElements() > negatives.getSumOfElements())
            {
                return positives.getAverage();
            }
            else if (negatives.getSumOfElements() > positives.getSumOfElements())
            {
                return negatives.getAverage();
            }
            else return 0;
        }
    }
    
    /**
     * Returns the getSumOfElements of all the numbers stored in the accumulator
     * @return getSumOfElements of all the numbers stored in the accumulator, as a double
     */
    public double getSumOfElements()
    {
        double s = 0d;
        s = _internal.stream().map((item) -> item.doubleValue()).reduce(s, (accumulator, _item) -> accumulator + _item);
        return s;
    }
    
    /**
     * Returns the number of elements in the accumulator
     * @return number of elements in the accumulator, as an integer
     */
    public int getCountOfElements()
    {
        return _internal.size();
    }
    
    public T max()
    {
        return this.max ;
    }
    
    public T min()
    {
        return this.min;
    }

    @Override
    public int compare(T o1, T o2) {
        return (int) (o1.doubleValue() - o2.doubleValue()) ;
    }
   
    
}

