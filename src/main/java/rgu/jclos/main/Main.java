package rgu.jclos.main;

import java.util.Map;
import rgu.jclos.redditanalysis.SentimentAnalyzer;
import rgu.jclos.redditanalysis.Comment;
import rgu.jclos.redditanalysis.RedditListener;

/**
 *
 * @author Jeremie Clos
 */
public class Main {
    

    /**
     * Main method
     *
     * @param args command-line parameter
     */
    public static void main(String[] args) throws InterruptedException {
        RedditListener rl = new RedditListener(10000, 1000, "all");
        rl.update();
        SentimentAnalyzer a = new SentimentAnalyzer(rl);
        while (true)
        {
            Thread.sleep(1000);//sleep 1 seconds
            rl.update();
            System.out.println("Average sentiment is " + a.getAverageSentiment() + " (based on " + rl._commentMemory.size() + " comments)");
        }

    }
}
