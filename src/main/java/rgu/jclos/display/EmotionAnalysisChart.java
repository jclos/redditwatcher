/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.jclos.display;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Timer;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import rgu.jclos.redditanalysis.EmotionAnalyzer;
import rgu.jclos.redditanalysis.EmotionProfile;
import rgu.jclos.redditanalysis.RedditListener;
/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class EmotionAnalysisChart extends ApplicationFrame implements ActionListener {
    /** The time series data. */
    private TimeSeries angerSeries;
    private TimeSeries fearSeries;
    private TimeSeries sadSeries;
    private TimeSeries joySeries;
    private TimeSeries loveSeries;
    private TimeSeries surpriseSeries;
    
    
    protected RedditListener rl ;
    protected EmotionAnalyzer a ;


    /** Timer to refresh graph after every 1/4th of a second */
    private Timer timer = new Timer(250, this);
    private Thread crawlerProcess ;
    /**
     * Constructs a new dynamic chart application.
     *
     * @param title  the frame title.
     */
    public EmotionAnalysisChart(final String title) throws InterruptedException, IOException {
        super(title);
        
        // Lambda Runnable
        Runnable crawler = () -> {
            try {
                rl = new RedditListener(10000, 10000, "news", "politics", "unitedkingdom", "worldnews", "technology", "fitness", "angry", "happy", "tech", "android", "apple", "funny", "jokes");
                rl.update();
                a = new EmotionAnalyzer(rl);
            } catch (IOException ex) {
                Logger.getLogger(EmotionAnalysisChart.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(EmotionAnalysisChart.class.getName()).log(Level.SEVERE, null, ex);
            }
        };

        // start the thread
        crawlerProcess = new Thread(crawler);
        crawlerProcess.start();
        
        //forces the app to wait so that the EmotionAnalyzer has been instantiated
        synchronized(this) {
            while (a == null) Thread.sleep(1000);
        }
        
//        rl = new RedditListener(10000, 10000, "news", "politics", "unitedkingdom", "worldnews", "technology", "fitness", "angry");
//        rl.update();
//        a = new EmotionAnalyzer(rl);

        this.angerSeries = new TimeSeries("Average Anger Level", Millisecond.class);
        this.fearSeries = new TimeSeries("Average Fear Level", Millisecond.class);
        this.sadSeries = new TimeSeries("Average Sadness Level", Millisecond.class);
        this.joySeries = new TimeSeries("Average Joy Level", Millisecond.class);
        this.loveSeries = new TimeSeries("Average Love Level", Millisecond.class);
        this.surpriseSeries = new TimeSeries("Average Surprise Level", Millisecond.class);

        final TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.addSeries(angerSeries);
        dataset.addSeries(fearSeries);
        dataset.addSeries(sadSeries);
        dataset.addSeries(joySeries);
        dataset.addSeries(loveSeries);
        dataset.addSeries(surpriseSeries);
        
        
        
        final JFreeChart chart = createChart(dataset);

        
        
        timer.setInitialDelay(1000);

        //Sets background color of chart
        chart.setBackgroundPaint(Color.LIGHT_GRAY);
        
        //Created JPanel to show graph on screen
        final JPanel content = new JPanel(new BorderLayout());

        //Created Chartpanel for chart area
        final ChartPanel chartPanel = new ChartPanel(chart);

        //Added chartpanel to main panel
        content.add(chartPanel);

        //Sets the size of whole window (JPanel)
        chartPanel.setPreferredSize(new java.awt.Dimension(1000, 500));

        //Puts the whole content on a Frame
        setContentPane(content);

        timer.start();
        
    }

    /**
     * Creates a sample chart.
     *
     * @param dataset  the dataset.
     *
     * @return A sample chart.
     */
    private JFreeChart createChart(final XYDataset dataset) {
        final JFreeChart result = ChartFactory.createTimeSeriesChart(
            "Emotion Analysis on Reddit",
            "Time",
            "Average Level",
            dataset,
            true,
            true,
            false
        );

        final XYPlot plot = result.getXYPlot();

        
        
        plot.setBackgroundPaint(new Color(0xffffe0));
        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.lightGray);
        
        for (int i = 0; i < 6; i++) // for each time series
        {
            plot.getRenderer().setSeriesStroke(i, new BasicStroke(2.5f));
        }
        

        ValueAxis xaxis = plot.getDomainAxis();
        xaxis.setAutoRange(true);
        xaxis.setFixedAutoRange(86400000);  // 20 minutes
        xaxis.setVerticalTickLabels(false);
        ValueAxis yaxis = plot.getRangeAxis();
        yaxis.setAutoRange(true);

        return result;
    }
    /**
     *
     * @param e  the action event.
     */
    public void actionPerformed(final ActionEvent e) {

        try {
            rl.update();
        } catch (InterruptedException ex) {
            Logger.getLogger(EmotionAnalysisChart.class.getName()).log(Level.SEVERE, null, ex);
        }

        final Millisecond now = new Millisecond();
        EmotionProfile p;
        
        try {
            if (a == null) System.out.println("ALERT! ALERT! SHIT'S ON FIRE!");
            p = a.getAverageProfile();
            this.angerSeries.add(now, p._anger);
            this.fearSeries.add(now, p._fear);
            this.sadSeries.add(now, p._sad);
            this.joySeries.add(now, p._joy);
            this.loveSeries.add(now, p._love);
            this.surpriseSeries.add(now, p._surprise);
            
            
        } catch (ExecutionException ex) {
            Logger.getLogger(EmotionAnalysisChart.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("[Analysis done on " + rl._commentMemory.size() + " comments]");
    }

    /**
     * Starting point for the dynamic graph application.
     *
     * @param args  ignored.
     */
    public static void main(final String[] args) throws InterruptedException, IOException {

        final EmotionAnalysisChart demo = new EmotionAnalysisChart("Reddit Emotion Analysis");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
        demo.setResizable(false);
    }
}
