/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.jclos.display;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Timer;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import rgu.jclos.redditanalysis.SentimentAnalyzer;
import rgu.jclos.redditanalysis.RedditListener;
/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class SentimentAnalysisChart extends ApplicationFrame implements ActionListener {
    /** The time series data. */
    private TimeSeries series;
    
    private RedditListener rl ;
    private SentimentAnalyzer a ;
    /** The most recent value added. */
    private double lastValue = 100.0;

    /** Timer to refresh graph after every 1/4th of a second */
    private Timer timer = new Timer(10000, this);

    /**
     * Constructs a new dynamic chart application.
     *
     * @param title  the frame title.
     */
    public SentimentAnalysisChart(final String title) throws InterruptedException {
        
        super(title);
        
        // Lambda Runnable
        Runnable crawler = () -> {
            try {
                rl = new RedditListener(10000, 10000, "news", "politics", "unitedkingdom", "worldnews", "technology", "fitness", "angry");
                rl.update();
                a = new SentimentAnalyzer(rl);
            } catch (InterruptedException ex) {
                Logger.getLogger(EmotionAnalysisChart.class.getName()).log(Level.SEVERE, null, ex);
            }
        };

        // start the thread
        Thread t = new Thread(crawler);
        t.start();
        
        this.series = new TimeSeries("Average Sentiment", Millisecond.class);

        final TimeSeriesCollection dataset = new TimeSeriesCollection(this.series);
        final JFreeChart chart = createChart(dataset);

        timer.setInitialDelay(1000);

        //Sets background color of chart
        chart.setBackgroundPaint(Color.LIGHT_GRAY);

        //Created JPanel to show graph on screen
        final JPanel content = new JPanel(new BorderLayout());

        //Created Chartpanel for chart area
        final ChartPanel chartPanel = new ChartPanel(chart);

        //Added chartpanel to main panel
        content.add(chartPanel);

        //Sets the size of whole window (JPanel)
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 500));

        //Puts the whole content on a Frame
        setContentPane(content);

        timer.start();

    }

    /**
     * Creates a sample chart.
     *
     * @param dataset  the dataset.
     *
     * @return A sample chart.
     */
    private JFreeChart createChart(final XYDataset dataset) {
        final JFreeChart result = ChartFactory.createTimeSeriesChart(
            "Sentiment Analysis on Reddit",
            "Time",
            "Average Sentiment",
            dataset,
            true,
            true,
            false
        );

        final XYPlot plot = result.getXYPlot();

        plot.setBackgroundPaint(new Color(0xffffe0));
        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.lightGray);

        ValueAxis xaxis = plot.getDomainAxis();
        xaxis.setAutoRange(true);

        //Domain axis would show data of 60 seconds for a time
        xaxis.setFixedAutoRange(240000.0);  // 240 seconds
        xaxis.setVerticalTickLabels(false);
        
        ValueAxis yaxis = plot.getRangeAxis();
        yaxis.setRange(-1.0, 1.0);

        return result;
    }
    /**
     * Generates an random entry for a particular call made by time for every 1/4th of a second.
     *
     * @param e  the action event.
     */
    public void actionPerformed(final ActionEvent e) {

        this.lastValue = a.getAverageSentiment();


        try {
            rl.update();
        } catch (InterruptedException ex) {
            Logger.getLogger(SentimentAnalysisChart.class.getName()).log(Level.SEVERE, null, ex);
        }


        final Millisecond now = new Millisecond();
        this.series.add(now, this.lastValue);

        System.out.println("Analysis done on " + rl._commentMemory.size() + " comments");
    }

    /**
     * Starting point for the dynamic graph application.
     *
     * @param args  ignored.
     */
    public static void main(final String[] args) throws InterruptedException {

        final SentimentAnalysisChart demo = new SentimentAnalysisChart("Reddit Sentiment Analysis");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
        demo.setResizable(false);
    }
}
