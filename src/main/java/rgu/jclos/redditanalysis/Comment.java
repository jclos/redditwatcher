package rgu.jclos.redditanalysis;

/**
 *
 * @author Jeremie Clos
 */
public class Comment {
    
    public final String _id ;
    public final String _body ;
    public final String _htmlBody ;
    public final String _subreddit ;
    public final int _score ;
    public final String _link_title ;
    public final String _link_url ;
    
    public Comment(String id, String txtBody, String htmlBody, String subreddit, int score, String link_title, String link_url)
    {
        _id = id ;
        _body = txtBody ;
        _htmlBody = htmlBody ;
        _subreddit = subreddit ;
        _score = score ;
        _link_title = link_title ;
        _link_url = link_url ;
    }
}
