/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package rgu.jclos.redditanalysis ;

/**
 * Refactored using Java NIO for faster read times on disk, 
 * lambdas and parallel streams for faster classification (average 2.8x speed up)
 * Requires Java 8
 * @author Harsha
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.DoubleAdder;

public class EmoAnalytics {

    public final static String lexiconFile ="R:\\R-Smartweb-Data-Sets\\AnilBandhakavi\\moreTwitter\\tweetdataset\\biglex\\Lexicon-1.txt" ;
    
    public final static Map<String, String> readLexicon(String filename) throws FileNotFoundException, IOException {
        Map<String, String> EmoLexicon = new HashMap<>();
        Files.readAllLines(new File(filename).toPath()).stream().forEach(line -> {
            String[] values = line.split("\t");
            EmoLexicon.put(values[0], values[1] + "\t" + values[2] + "\t" + values[3] + "\t" + values[4] + "\t" + values[5] + "\t" + values[6] + "\t" + values[7]);
        });
        return EmoLexicon ;
    }
    
    public static void main(String[] args) throws IOException {
        String test = "Finally got over my fear from a terrible dentist as a teenager, and at 28 went to get stuff fixed. Slowly but surely it's getting better. Today I finally had a front top tooth crowned like the others around it, and my confidence after looking in the mirror is already boosted. Next is getting the wisdom teeth out and a few more small fillings. I've found an awesome dentist and I'm extremely happy. Just wanted to share.";
        Map<String, String> lex = readLexicon(lexiconFile);
        EmotionProfile p = analyze(test, lex);
        System.out.println(p._anger);
        System.out.println(p._fear);
        System.out.println(p._joy);
        System.out.println(p._love);
        System.out.println(p._surprise);
        System.out.println(p._sad);
        System.out.println(p._neutral);
    }
    
    public final static EmotionProfile analyze(String text, Map<String, String> EmoLexicon) {
        Map<String, Double> emotionalVector = getEmotionalProfile(text, EmoLexicon);
        if (emotionalVector != null && !emotionalVector.isEmpty()) {
            double sum = 0 ;
            for (double value : emotionalVector.values()) sum += value ;
           
            double anger = emotionalVector.get("anger") / sum;
            double fear = emotionalVector.get("fear") / sum;
            double sad = emotionalVector.get("sad") / sum;
            double surprise = emotionalVector.get("surprise") / sum;
            double joy = emotionalVector.get("joy") / sum;
            double love = emotionalVector.get("love") / sum;
            double neutral = emotionalVector.get("neutral") / sum ;

            return new EmotionProfile(anger, fear, sad, surprise, joy, love, neutral);
        }
        return new EmotionProfile(0,0,0,0,0,0,1);
    }

    public final static Map<String, Double> getEmotionalProfile(String text, Map<String, String> emoLexicon) {
        Map<String, Double> emotionalVector = new HashMap<>();
        String[] words = text.split(" ");
        DoubleAdder angerScore = new DoubleAdder();
        DoubleAdder fearScore= new DoubleAdder();
        DoubleAdder joyScore = new DoubleAdder();
        DoubleAdder sadScore = new DoubleAdder();
        DoubleAdder surpriseScore = new DoubleAdder();
        DoubleAdder loveScore = new DoubleAdder();
        DoubleAdder neutralScore = new DoubleAdder();
        Arrays.asList(words).stream().parallel().forEach(word -> {
            if (emoLexicon.containsKey(word)) {
                String[] scores = emoLexicon.get(word).split("\t");
                angerScore.add(Double.parseDouble(scores[0]));
                fearScore.add(Double.parseDouble(scores[1]));
                joyScore.add(Double.parseDouble(scores[2]));
                sadScore.add(Double.parseDouble(scores[3]));
                surpriseScore.add(Double.parseDouble(scores[4]));
                loveScore.add(Double.parseDouble(scores[5]));
                neutralScore.add(Double.parseDouble(scores[6]));
            }
        }) ;
        emotionalVector.put("anger", angerScore.doubleValue());
        emotionalVector.put("fear", fearScore.doubleValue());
        emotionalVector.put("joy", joyScore.doubleValue());
        emotionalVector.put("sad", sadScore.doubleValue());
        emotionalVector.put("surprise", surpriseScore.doubleValue());
        emotionalVector.put("love", loveScore.doubleValue());
        emotionalVector.put("neutral", neutralScore.doubleValue());
        return emotionalVector;
    }
}
