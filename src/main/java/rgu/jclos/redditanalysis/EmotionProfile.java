/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.jclos.redditanalysis;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class EmotionProfile {

    public final double _anger;
    public final double _fear;
    public final double _joy;
    public final double _sad;
    public final double _surprise;
    public final double _love;
    public final double _neutral ;

    public EmotionProfile(double anger, double fear, double joy, double sad, double surprise, double love, double neutral) {
        _anger = anger;
        _fear = fear;
        _joy = joy;
        _sad = sad;
        _surprise = surprise;
        _love = love;
        _neutral = neutral ;
    }

    public double sum() {
        return _anger + _fear + _joy + _sad + _surprise + _love + _neutral;
    }
}
