/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.jclos.redditanalysis;

import rgu.jclos.util.JsonReader;
import com.jayway.jsonpath.JsonPath;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jeremie Clos
 */
public class RedditListener {

    public final int waitingPeriod;
    public boolean filter = false;
    public final Map<String, Comment> _commentMemory;
    public final int _memoryLimit;
    public Map<Integer, Double> evolution;
    private Set<String> subredditsToFilter;
    private Set<String> termFilters;

    public void addTermFilters(String... terms) {
        for (String term : terms) {
            termFilters.add(term);
        }
    }

    public RedditListener(int waitFor, String... subreddits) {
        termFilters = new HashSet<>();
        waitingPeriod = waitFor;
        if (subreddits.length > 0) {
            filter = true;
            subredditsToFilter = new HashSet<>();
            subredditsToFilter.addAll(Arrays.asList(subreddits));
            subredditsToFilter.forEach(sub -> System.out.println("Watching comments from " + sub));
        }
        _commentMemory = new HashMap<>();
        _memoryLimit = -1;
        evolution = new TreeMap<>();
        System.getProperties().put("http.proxyHost", Configuration.PROXY_URL);
        System.getProperties().put("http.proxyPort", Configuration.PROXY_PORT);
    }

    /**
     * Will wipe the memory once it reaches the limit
     *
     * @param memoryLimit
     */
    public RedditListener(int memoryLimit, int waitFor, String... subreddits) {
        termFilters = new HashSet<>();
        waitingPeriod = waitFor;
        if (subreddits.length > 0) {
            filter = true;
            subredditsToFilter = new HashSet<>();
            subredditsToFilter.addAll(Arrays.asList(subreddits));
            subredditsToFilter.forEach(sub -> System.out.println("Watching comments from " + sub));
        }
        _commentMemory = new HashMap<>();
        _memoryLimit = memoryLimit;
        evolution = new TreeMap<>();
        System.getProperties().put("http.proxyHost", Configuration.PROXY_URL);
        System.getProperties().put("http.proxyPort", Configuration.PROXY_PORT);
    }

    public void update() throws InterruptedException {
        try {
            if (filter) {
                for (String sr : subredditsToFilter) {
                    Thread.sleep(waitingPeriod);
                    String url = Configuration.SUBREDDIT_COMMENT_FLOW(sr);
                    JSONObject newList = JsonReader.readJsonFromUrl(url);
                    String list = newList.toString();
                    List<Object> comments = JsonPath.read(list, "$.data.children[*].data");
                    System.out.println("Received " + comments.size() + " comments from /r/" + sr);
                    for (Object comment : comments) {
                        String htmlBody = JsonPath.read(comment.toString(), "$.body_html");
                        String txtBody = JsonPath.read(comment.toString(), "$.body");
                        String subreddit = JsonPath.read(comment.toString(), "$.subreddit");
                        String id = JsonPath.read(comment.toString(), "$.id");
                        int score = JsonPath.read(comment.toString(), "$.score");
                        String link_title = JsonPath.read(comment.toString(), "$.link_title");
                        String link_url = JsonPath.read(comment.toString(), "$.link_url");
                        if (subredditsToFilter.contains(subreddit.toLowerCase())) {
                            if (!_commentMemory.containsKey(id)) {
                                if (!termFilters.isEmpty()) {
                                    filterloop:
                                    for (String sfilter : termFilters) {
                                        if (txtBody.toLowerCase().contains(sfilter.toLowerCase().trim())) {
                                            _commentMemory.put(id, new Comment(id, txtBody, htmlBody, subreddit, score, link_title, link_url));
                                            break filterloop;
                                        }
                                    }
                                } else {
                                    _commentMemory.put(id, new Comment(id, txtBody, htmlBody, subreddit, score, link_title, link_url));
                                }
                            }
                        }
                    }
                }
            } else {
                JSONObject newList = JsonReader.readJsonFromUrl(Configuration.COMMENTS_FLOW);
                String list = newList.toString();
                List<Object> comments = JsonPath.read(list, "$.data.children[*].data");
                System.out.println("Received " + comments.size() + " comments");
                for (Object comment : comments) {
                    String htmlBody = JsonPath.read(comment.toString(), "$.body_html");
                    String txtBody = JsonPath.read(comment.toString(), "$.body");
                    String subreddit = JsonPath.read(comment.toString(), "$.subreddit");
                    String id = JsonPath.read(comment.toString(), "$.id");
                    int score = JsonPath.read(comment.toString(), "$.score");
                    String link_title = JsonPath.read(comment.toString(), "$.link_title");
                    String link_url = JsonPath.read(comment.toString(), "$.link_url");
                    if (!_commentMemory.containsKey(id)) {
                        if (!termFilters.isEmpty()) {
                            filterloop:
                            for (String sfilter : termFilters) {
                                if (txtBody.toLowerCase().contains(sfilter.toLowerCase().trim())) {
                                    _commentMemory.put(id, new Comment(id, txtBody, htmlBody, subreddit, score, link_title, link_url));
                                    break filterloop;
                                }
                            }
                        } else {
                            _commentMemory.put(id, new Comment(id, txtBody, htmlBody, subreddit, score, link_title, link_url));
                        }
                    }

                }
            }
        } catch (IOException | JSONException ex) {
            Logger.getLogger(RedditListener.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (_memoryLimit != -1 && _commentMemory.size() > _memoryLimit) {
                _commentMemory.clear();
            }
        }
    }
}
