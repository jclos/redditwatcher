package rgu.jclos.redditanalysis;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import rgu.jclos.util.Accumulator;
import smartsa.SmartSA;

/**
 *
 * @author Jeremie Clos
 */
public class SentimentAnalyzer {
    
    private final RedditListener _listener ;
    private final SmartSA _smartSA ;
    private final LoadingCache<String, Double> sentimentCache ;
    
    
    public SentimentAnalyzer(RedditListener listener)
    {
        _listener = listener ;
        _smartSA = new SmartSA();
        sentimentCache = CacheBuilder.newBuilder()
                .maximumSize(_listener._memoryLimit)
                .build(
                        new CacheLoader<String, Double>() {
                            @Override
                            public Double load(String key) throws Exception {
                                return getSentiment(key);
                            }
                        }); 
    }
    
    private double getSentiment(String comment)
    {
        double[] sents = _smartSA.getSentimentScores(comment);
        return sents[0] - sents[1];
    }
    
    public double getAverageSentiment()
    {
        Accumulator<Double> sentiments = new Accumulator<>();
        for (Comment c : _listener._commentMemory.values())
        {
            sentiments.add(getSentiment(c._body));
        }
        if (sentiments.isEmpty()) return 0d; 
        else return sentiments.getAverage();
    }
    
}
