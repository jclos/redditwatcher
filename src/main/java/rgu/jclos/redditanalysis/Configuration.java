package rgu.jclos.redditanalysis;

/**
 *
 * @author Jeremie Clos
 */
public class Configuration {
    public static final String PROXY_URL = "proxy.rgu.ac.uk";
    public static final String PROXY_PORT = "8080";
    public static final String COMMENTS_FLOW = "http://www.reddit.com/r/all/comments/.json?limit=100";
    public static final String STORIES_FLOW = "http://www.reddit.com/r/all/.json?limit=100";
    public static String SUBREDDIT_COMMENT_FLOW(String subreddit) {return "http://www.reddit.com/r/"+ subreddit + "/comments/.json?limit=100";};
    public static String SUBREDDIT_STORIES_FLOW(String subreddit) {return "http://www.reddit.com/r/"+ subreddit + "/.json?limit=100";};
    
    public static final String[] SCOTTISH_SUBREDDITS = {"scotland", "aberdeen", "edinburgh", "fife", "dundee"} ;
    public static final String[] EUROPEAN_SUBREDDITS = {"europe", "unitedkingdom", "germany", "france", "spain", "italy"};
}
