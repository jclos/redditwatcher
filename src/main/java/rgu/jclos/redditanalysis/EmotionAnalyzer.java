package rgu.jclos.redditanalysis;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.DoubleAccumulator;
import java.util.logging.Level;
import java.util.logging.Logger;
import rgu.jclos.util.Accumulator;

/**
 *
 * @author Jeremie Clos
 */
public class EmotionAnalyzer {

    private final RedditListener _listener;
    private final LoadingCache<String, EmotionProfile> emoCache;

    private final Map<String, String> lexicon;

    public EmotionAnalyzer(RedditListener listener) throws IOException {
        _listener = listener;
        lexicon = EmoAnalytics.readLexicon(EmoAnalytics.lexiconFile);
        emoCache = CacheBuilder.newBuilder()
                .maximumSize(_listener._memoryLimit)
                .build(
                        new CacheLoader<String, EmotionProfile>() {
                            @Override
                            public EmotionProfile load(String key) throws Exception {
                                return getProfile(key);
                            }
                        });
    }

    private EmotionProfile getProfile(String comment) throws ExecutionException {
        return EmoAnalytics.analyze(comment, lexicon);
    }

    public EmotionProfile getAverageProfile() throws ExecutionException {
        DoubleAccumulator angerAcc = new DoubleAccumulator((n1, n2) -> n1 + n2, 0d);
        DoubleAccumulator fearAcc = new DoubleAccumulator((n1, n2) -> n1 + n2, 0d);
        DoubleAccumulator sadAcc = new DoubleAccumulator((n1, n2) -> n1 + n2, 0d);
        DoubleAccumulator joyAcc = new DoubleAccumulator((n1, n2) -> n1 + n2, 0d);
        DoubleAccumulator loveAcc = new DoubleAccumulator((n1, n2) -> n1 + n2, 0d);
        DoubleAccumulator surpriseAcc = new DoubleAccumulator((n1, n2) -> n1 + n2, 0d);
        DoubleAccumulator neutralAcc = new DoubleAccumulator((n1, n2) -> n1 + n2, 0d);

        int counter = _listener._commentMemory.values().size();
//        Accumulator<Double> anger = new Accumulator<>();
//        Accumulator<Double> fear = new Accumulator<>();
//        Accumulator<Double> sad = new Accumulator<>();
//        Accumulator<Double> joy = new Accumulator<>();
//        Accumulator<Double> love = new Accumulator<>();
//        Accumulator<Double> surprise = new Accumulator<>();
//        Accumulator<Double> neutral = new Accumulator<>();
        _listener._commentMemory.values().stream().parallel().forEach(c -> {
            try {
                EmotionProfile p = this.getProfile(c._body);
//            anger.add(p._anger, n -> n >= 0);
//            fear.add(p._fear, n -> n >= 0);
//            sad.add(p._sad, n -> n >= 0);
//            joy.add(p._joy, n -> n >= 0);
//            love.add(p._love, n -> n >= 0);
//            surprise.add(p._surprise, n -> n >= 0);
//            neutral.add(p._neutral, n -> n >= 0);
                if (p._anger >= 0) {
                    angerAcc.accumulate(p._anger);
                }
                if (p._fear >= 0) {
                    fearAcc.accumulate(p._fear);
                }
                if (p._sad >= 0) {
                    sadAcc.accumulate(p._sad);
                }
                if (p._love >= 0) {
                    loveAcc.accumulate(p._love);
                }
                if (p._joy >= 0) {
                    joyAcc.accumulate(p._joy);
                }
                if (p._surprise >= 0) {
                    surpriseAcc.accumulate(p._surprise);
                }
                if (p._neutral >= 0) {
                    neutralAcc.accumulate(p._neutral);
                }
            } catch (ExecutionException ex) {
                System.out.println("BOOP BIP BOOP");
                ex.printStackTrace();
                Logger.getLogger(EmotionAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        //double anger, double fear, double joy, double sad, double surprise, double love
        return new EmotionProfile(
                angerAcc.doubleValue() / counter,
                fearAcc.doubleValue() / counter,
                joyAcc.doubleValue() / counter,
                sadAcc.doubleValue() / counter,
                surpriseAcc.doubleValue() / counter,
                loveAcc.doubleValue() / counter,
                neutralAcc.doubleValue() / counter
        );
    }
}
